import 'dart:math';

import 'package:flutter/material.dart';

class RadiasProgres extends StatefulWidget {
  final double porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;

  RadiasProgres(
      {@required this.porcentaje,
      this.colorPrimario = Colors.blue,
      this.colorSecundario = Colors.grey,
      this.grosorPrimario = 10.0,
      this.grosorSecundario = 4.0});

  @override
  _RadiasProgresState createState() => _RadiasProgresState();
}

class _RadiasProgresState extends State<RadiasProgres>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  double porcentajeAnt = 0.0;

  @override
  void initState() {
    porcentajeAnt = widget.porcentaje;

    controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 200));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    controller.forward(from: 0.0);
    final diffAnimar = widget.porcentaje - porcentajeAnt;
    porcentajeAnt = widget.porcentaje;

    return AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child) {
          return Container(
              width: double.infinity,
              height: double.infinity,
              padding: EdgeInsets.all(10),
              child: CustomPaint(
                painter: _MiRadialProgres(
                    porcentaje: (widget.porcentaje - diffAnimar) +
                        (diffAnimar * controller.value),
                    colorPrimario: widget.colorPrimario,
                    colorSecundario: widget.colorSecundario,
                    grosorPrimario: widget.grosorPrimario,
                    grosorSecundario: widget.grosorSecundario),
              ));
        });

    /*  */
  }
}

class _MiRadialProgres extends CustomPainter {
  final double porcentaje;
  final Color colorPrimario;
  final Color colorSecundario;
  final double grosorPrimario;
  final double grosorSecundario;
  _MiRadialProgres(
      {this.porcentaje,
      this.colorPrimario,
      this.colorSecundario,
      this.grosorPrimario,
      this.grosorSecundario});

  @override
  void paint(Canvas canvas, Size size) {
    //Circulo completado
    final paint = new Paint()
      ..strokeWidth = this.grosorSecundario
      ..color = colorSecundario
      ..style = PaintingStyle.stroke;

    Offset c = Offset(size.height * 0.5, size.width * 0.5);
    double radius = min(size.width * 0.5, size.height * 0.5);

    canvas.drawCircle(c, radius, paint);

    //arco
    final paintArco = new Paint()
      ..strokeWidth = this.grosorPrimario
      ..strokeCap = StrokeCap.round
      ..color = colorPrimario
      ..style = PaintingStyle.stroke;

    //Parte que se debera ir llenando
    double archAngle = 2 * pi * (porcentaje / 100);

    canvas.drawArc(Rect.fromCircle(center: c, radius: radius), (-pi / 2),
        archAngle, false, paintArco);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
