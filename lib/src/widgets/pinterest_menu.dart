import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButton {
  final Function onPress;
  final IconData icon;

  PinterestButton({@required this.onPress, @required this.icon});
}

class PinteresMenu extends StatelessWidget {
  final bool mostrar;
  final Color activeColor;
  final Color backgroundColor;
  final Color inactiveColor;
  final List<PinterestButton> items;

  PinteresMenu(
      {this.mostrar = true,
      this.activeColor = Colors.black,
      this.backgroundColor = Colors.white,
      this.inactiveColor = Colors.blueGrey,
      @required this.items});

  /* final List<PinterestButton> items = [
    PinterestButton(
        icon: Icons.pie_chart,
        onPress: () {
          print('Icon pie_chart');
        }),
    PinterestButton(
        icon: Icons.search,
        onPress: () {
          print('Icon search');
        }),
    PinterestButton(
        icon: Icons.notifications,
        onPress: () {
          print('Icon notifications');
        }),
    PinterestButton(
        icon: Icons.supervised_user_circle,
        onPress: () {
          print('Icon supervised_user_circle');
        })
  ]; */

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ChangeNotifierProvider(
          create: (_) => new _MenuModel(),
          child: Builder(builder: (BuildContext context) {
            Provider.of<_MenuModel>(context).activeColor = this.activeColor;
            Provider.of<_MenuModel>(context).inactiveColor = this.inactiveColor;
            Provider.of<_MenuModel>(context).backgroundColor =
                this.backgroundColor;
            return AnimatedOpacity(
                duration: Duration(milliseconds: 250),
                opacity: (mostrar) ? 1 : 0,
                child: _PinteresMenuBackgound(child: _MenuItems(items)));
          })),
    );
  }
}

class _PinteresMenuBackgound extends StatelessWidget {
  final Widget child;

  const _PinteresMenuBackgound({@required this.child});

  @override
  Widget build(BuildContext context) {
    final bg = Provider.of<_MenuModel>(context).backgroundColor;
    return Container(
      child: this.child,
      width: 250,
      height: 60,
      decoration: BoxDecoration(
          color: bg,
          borderRadius: BorderRadius.all(Radius.circular(100)),
          boxShadow: <BoxShadow>[
            BoxShadow(color: Colors.black38, blurRadius: 10, spreadRadius: -5)
          ]),
    );
  }
}

class _MenuItems extends StatelessWidget {
  final List<PinterestButton> menuItems;

  _MenuItems(this.menuItems);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(this.menuItems.length,
          (index) => _PinterestMenuButton(index, menuItems[index])),
    );
  }
}

class _PinterestMenuButton extends StatelessWidget {
  final int index;
  final PinterestButton item;

  _PinterestMenuButton(this.index, this.item);

  @override
  Widget build(BuildContext context) {
    final itemSeleccionado = Provider.of<_MenuModel>(context).itemSeleccionado;

    final activeColor = Provider.of<_MenuModel>(context).activeColor;
    final inactiveColor = Provider.of<_MenuModel>(context).inactiveColor;

    return GestureDetector(
      onTap: () {
        Provider.of<_MenuModel>(context, listen: false).itemSeleccionado =
            index;
        print(index);
        item.onPress();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: Icon(
          item.icon,
          size: (itemSeleccionado == index) ? 30 : 25,
          color: (itemSeleccionado == index) ? activeColor : inactiveColor,
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  int _itemSeleccionado = 0;
  Color _activeColor = Colors.black;
  Color _backgroundColor = Colors.white;
  Color _inactiveColor = Colors.blueGrey;

  int get itemSeleccionado => this._itemSeleccionado;

  set itemSeleccionado(int i) {
    this._itemSeleccionado = i;
    notifyListeners();
  }

  Color get activeColor => this._activeColor;
  set activeColor(Color ac) => this._activeColor = ac;

  Color get inactiveColor => this._inactiveColor;
  set inactiveColor(Color ic) => this._inactiveColor = ic;

  Color get backgroundColor => this._backgroundColor;
  set backgroundColor(Color bg) => this._backgroundColor = bg;
}
