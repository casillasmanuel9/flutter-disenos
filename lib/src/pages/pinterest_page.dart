import 'package:counter/src/theme/theme.dart';
import 'package:counter/src/widgets/pinterest_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class PinteresPage extends StatelessWidget {
  const PinteresPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
            /* PinteresMenu()  Container(
        child: PinterestGrid(),
      ), */
            ChangeNotifierProvider(
      create: (_) => _MenuModel(),
      child: Stack(
        children: [
          PinterestGrid(),
          _PinteresMenuLocation(),
        ],
      ),
    ));
  }
}

class _PinteresMenuLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;

    if (width > 500) {
      width = width - 300;
    }

    return Positioned(
      child: Container(
          width: width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PinteresMenu(
                items: [
                  PinterestButton(
                      icon: Icons.pie_chart,
                      onPress: () {
                        print('Icon pie_chart');
                      }),
                  PinterestButton(
                      icon: Icons.search,
                      onPress: () {
                        print('Icon search');
                      }),
                  PinterestButton(
                      icon: Icons.notifications,
                      onPress: () {
                        print('Icon notifications');
                      }),
                  PinterestButton(
                      icon: Icons.supervised_user_circle,
                      onPress: () {
                        print('Icon supervised_user_circle');
                      })
                ],
                mostrar: Provider.of<_MenuModel>(context).mostrar,
                backgroundColor: appTheme.scaffoldBackgroundColor,
                activeColor: appTheme.accentColor,
              ),
            ],
          )),
      bottom: 30,
    );
  }
}

class PinterestGrid extends StatefulWidget {
  @override
  _PinterestGridState createState() => _PinterestGridState();
}

class _PinterestGridState extends State<PinterestGrid> {
  final List<int> items = List.generate(200, (index) => index);
  ScrollController controller = new ScrollController();

  double scrollaAnterior = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      if (controller.offset > scrollaAnterior && controller.offset > 150) {
        Provider.of<_MenuModel>(context, listen: false).mostrar = false;
        print('ocultar menu');
      } else {
        Provider.of<_MenuModel>(context, listen: false).mostrar = true;
        print('mostrar menu');
      }

      scrollaAnterior = controller.offset;
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int count = 2;
    if (MediaQuery.of(context).size.width > 500) {
      count = 3;
    }
    return new StaggeredGridView.countBuilder(
      controller: controller,
      crossAxisCount: count,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) => _PinterestWidget(index),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(1, index.isEven ? 1 : 2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class _PinterestWidget extends StatelessWidget {
  final int index;
  _PinterestWidget(this.index);

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return new Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: appTheme.accentColor,
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: new Center(
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: new Text('$index'),
          ),
        ));
  }
}

class _MenuModel with ChangeNotifier {
  bool _mostrar = true;

  get mostrar => this._mostrar;

  set mostrar(bool m) {
    this._mostrar = m;
    notifyListeners();
  }
}
