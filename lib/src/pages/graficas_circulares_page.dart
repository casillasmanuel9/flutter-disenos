import 'package:counter/src/theme/theme.dart';
import 'package:counter/src/widgets/radial_progres.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GraficasCircularesPage extends StatefulWidget {
  const GraficasCircularesPage({Key key}) : super(key: key);

  @override
  _GraficasCircularesPageState createState() => _GraficasCircularesPageState();
}

class _GraficasCircularesPageState extends State<GraficasCircularesPage> {
  double porcentaje = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomRadialProgres(
                porcentaje: porcentaje,
                color: Colors.blue,
              ),
              CustomRadialProgres(
                porcentaje: porcentaje * 1.2,
                color: Colors.red,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CustomRadialProgres(
                  porcentaje: porcentaje * 1.4, color: Colors.pink),
              CustomRadialProgres(
                  porcentaje: porcentaje * 1.6, color: Colors.purple)
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            porcentaje += 10;
            if (porcentaje > 100) {
              porcentaje = 0;
            }
          });
        },
        child: Icon(Icons.refresh),
      ),
    );
  }
}

class CustomRadialProgres extends StatelessWidget {
  const CustomRadialProgres(
      {Key key, @required this.porcentaje, @required this.color})
      : super(key: key);

  final double porcentaje;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return Container(
      height: 150,
      width: 150,
      child: RadiasProgres(
        porcentaje: porcentaje,
        colorPrimario: color,
        colorSecundario: appTheme.textTheme.bodyText1.color,
        grosorPrimario: 10.0,
        grosorSecundario: 4.0,
      ),
    );
  }
}
