import 'package:counter/src/theme/theme.dart';
import 'package:counter/src/widgets/slideshow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class SlideShowPage extends StatelessWidget {
  const SlideShowPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isLarge = false;
    if (MediaQuery.of(context).size.height > 500) {
      isLarge = true;
    }
    final children = [Expanded(child: MySlides()), Expanded(child: MySlides())];
    return Scaffold(
      body: Center(
        child: (isLarge)
            ? Column(
                children: children,
              )
            : Row(
                children: children,
              ),
      ),
    );
  }
}

class MySlides extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = (appTheme.darkTheme)
        ? appTheme.currentTheme.accentColor
        : Color(0xffFF5A7E);
    return SlideShow(
      slides: <Widget>[
        SvgPicture.asset('assets/svgs/slide-1.svg'),
        SvgPicture.asset('assets/svgs/slide-2.svg'),
        SvgPicture.asset('assets/svgs/slide-3.svg'),
        SvgPicture.asset('assets/svgs/slide-4.svg'),
        SvgPicture.asset('assets/svgs/slide-5.svg'),
      ],
      primaryColor: accentColor,
      bulletPrimary: 15,
      bulletSecondary: 12,
    );
  }
}
