import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {
  bool _darkTheme = false;
  bool _customTheme = false;

  ThemeData _currentTheme;

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;
  ThemeData get currentTheme => this._currentTheme;

  ThemeChanger(int theme) {
    switch (theme) {
      case 1:
        this._darkTheme = false;
        this._customTheme = false;
        this._currentTheme = ThemeData.light();
        break;
      case 2:
        this._darkTheme = true;
        this._customTheme = false;
        this._currentTheme =
            ThemeData.dark().copyWith(accentColor: Colors.pink);
        break;

      case 2:
        this._darkTheme = false;
        this._customTheme = true;
        this._currentTheme = ThemeData.dark();
        break;
      default:
        this._darkTheme = false;
        this._customTheme = false;
        this._currentTheme = ThemeData.light();
        break;
    }
  }

  set darkTheme(bool value) {
    _customTheme = false;
    _darkTheme = value;
    if (value) {
      this._currentTheme = ThemeData.dark().copyWith(accentColor: Colors.pink);
    } else {
      this._currentTheme = ThemeData.light();
    }
    notifyListeners();
  }

  set customTheme(bool value) {
    _darkTheme = false;
    _customTheme = value;
    if (value) {
      this._currentTheme = ThemeData.dark().copyWith(
          accentColor: Color(0xff48A0EB),
          primaryColorLight: Colors.white,
          scaffoldBackgroundColor: Color(0xff16202B),
          textTheme: TextTheme(bodyText1: TextStyle(color: Colors.white)));
    } else {
      this._currentTheme = ThemeData.light();
    }
    notifyListeners();
  }
}
