import 'package:counter/src/pages/slideshow_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:counter/src/pages/emergency_page.dart';
import 'package:counter/src/pages/graficas_circulares_page.dart';
import 'package:counter/src/pages/headers_page.dart';
import 'package:counter/src/pages/pinterest_page.dart';
import 'package:counter/src/pages/sliver_page.dart';
import 'package:counter/src/retos/CuadradoAnimado_page.dart';

final pageRoutes = <_Route>[
  _Route(FontAwesomeIcons.slideshare, 'SlideShow', SlideShowPage()),
  _Route(FontAwesomeIcons.ambulance, 'Emergencia', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Encabezados', HeadersPage()),
  _Route(
      FontAwesomeIcons.peopleCarry, 'Cuadros animados', CuadradoAnimadoPage()),
  _Route(FontAwesomeIcons.circleNotch, ' Barra de progreso',
      GraficasCircularesPage()),
  _Route(FontAwesomeIcons.pinterest, 'Pinterest', PinteresPage()),
  _Route(FontAwesomeIcons.mobile, 'Slivers', SliverPage())
];

class _Route {
  final IconData icon;
  final String titulo;
  final Widget page;

  _Route(this.icon, this.titulo, this.page);
}
