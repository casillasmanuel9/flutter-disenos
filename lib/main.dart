/* import 'package:counter/src/pages/graficas_circulares_page.dart';
import 'package:counter/src/labs/slideshow_page.dart'; 
import 'package:counter/src/retos/CuadradoAnimado_page.dart';
import 'package:counter/src/pages/pinterest_page.dart';
import 'package:counter/src/pages/graficas_circulares_page.dart';
import 'package:counter/src/pages/emergency_page.dart';
import 'package:counter/src/pages/sliver_page.dart';
 */
import 'package:counter/src/models/layout_model.dart';
import 'package:counter/src/pages/launcher_page.dart';
import 'package:counter/src/pages/launcher_page_tablet.dart';
import 'package:counter/src/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeChanger>(create: (_) => ThemeChanger(2)),
        ChangeNotifierProvider<LayoutModel>(
          create: (_) => LayoutModel(),
        )
      ],
      child: MyApp(),
    ));

/* void main() => runApp(
    ChangeNotifierProvider(create: (_) => new ThemeChanger(2), child: MyApp()));
 */
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);

    return MaterialApp(
        theme: appTheme.currentTheme,
        title: 'Diseños App',
        debugShowCheckedModeBanner: false,
        home: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            //print('Orientacion $orientation');
            final screenSize = MediaQuery.of(context).size;
            if (screenSize.width > 500) {
              return LaucherTabletPage();
            } else {
              return LaucherPage();
            }
          },
        ));
  }
}
